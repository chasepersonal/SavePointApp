import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from '../../environments/environment';

// Decorator needed to inject modules into a service
// Components injectable; services are not
// Best practice to include in case injectables are needed in the future
@Injectable({
    providedIn: 'root'
})
export class AuthService {

    // Call API location for authorization
    baseUrl = environment.apiUrl + 'auth/';

    // Store user token in auth service as a property
    userToken: any;

    // Store decoded user token for parsing purposes
    decodedToken: any;

    // Set current token
    currentToken: any;

    jwtHelper: JwtHelperService = new JwtHelperService();

    // Inject Http service to provide a connection to the API
    constructor(private http: Http) { }

    // Call login method
    // Pass username and password through model as type any
    login(model: any) {
        // Return the reference of the Http post
        // Will contain information for the baseUrl with the login method, model data, and any options needed for authorization
        // Will be returned as an observable as type response (token object)
        // Will need to transform response into data that can be read by the browser (done with .map() operator)
        // Pass Headers through reference by calling this.requestOptions()
        return this.http.post(this.baseUrl + 'login', model, this.requestOptions()).pipe(map((response: Response)  => {
            const user = response.json();

            if (user && user.tokenString) {
                localStorage.setItem('token', user.tokenString);
                this.decodedToken = this.jwtHelper.decodeToken(user.tokenString);
                console.log(this.decodedToken);
                this.userToken = user.tokenString;
            }
        // Catch any errors that might arise from logging in
        }))
            .pipe(
                catchError(this.handleError)
            );
    }

    // Check if users token is expired when logging in
    // Done in auth services so that the work is not done in the navbar component
    // Push back to the navbar component once the token is checked
    loggedIn() {

        const token = localStorage.getItem('token');
        return  !!token && !this.jwtHelper.isTokenExpired(token);
    }

    register(model: any) {

        // Return the reference of the HTTP post that will contain the registration information
        // Catch any errors that might arise from any errors to the register page
        return this.http.post(this.baseUrl + 'register', model, this.requestOptions())
            .pipe(
                catchError(this.handleError)
            );
    }

    // Pass in option information for API
    private requestOptions() {
        const headers = new Headers({ 'Content-type': 'application/json' });
        return new RequestOptions({ headers: headers });
    }

    // Method to handle errors using the error type of any
    private handleError(error: any) {

        // Get application error from the header
        const applicationError = error.headers.get('Application-Error');

        // If application error exists, return an observable to throws the application error
        if (applicationError) {
            return throwError(applicationError);
        }

        // Extract errors in json format, then parse them into the error variable
        const serverError = error.json();

        // Variable to hold model state errors
        let modelStateErrors = '';

        if (serverError) {
            // Loop through keys in the serverError value
            for (const key in serverError) {
                // If key found, add it to the modelStateError's variable with the appropriate error for the key
                // Will add to a new line if there are multiple errors
                if (serverError[key]) {
                    modelStateErrors += serverError[key] + '\n';
                }
            }

            // Throw a message that either has the model state errors or a generic message if none are present
            return throwError(
                modelStateErrors || 'Server Error'
            );
        }
    }
}
