import { Injectable } from '@angular/core';

// Makes alertify available globally
declare let alertify: any;

@Injectable({
  providedIn: 'root'
})
export class AlertifyService {

    constructor() { }

    // Will take a message and run a callback after the confirm message has been confirmed
    confirm(message: string, okCallback: () => any) {
        // Get Alertify confirm message
        alertify.confirm(message, function (e) {
            // If an event function is passed, will trigger a callback message
            if (e) {
                okCallback();
            }
            // If no fuction is passed (cancel button clicked), no call back will happen
            else {

            }
        });
    }

    // Provide calls to alertify messages

    success(message: string) {
        alertify.success(message);
    }

    error(message: string) {
        alertify.error(message);
    }

    warning(message: string) {
        alertify.warning(message);
    }

    message(message: string) {
        alertify.message(message);
    }
}
