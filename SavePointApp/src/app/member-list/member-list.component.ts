import { Component, OnInit } from '@angular/core';
import { User } from '../_models/user';
import { UserService } from '../_services/user.service';
import { AlertifyService } from '../_services/alertify.service';

@Component({
    selector: 'app-member-list',
    templateUrl: './member-list.component.html',
    styleUrls: ['./member-list.component.css']
})
export class MemberListComponent implements OnInit {

    // Array to hold all users
    users: User[];

    constructor(private userService: UserService, private alertify: AlertifyService) { }

    ngOnInit() {
        // Loads users to page
        this.loadUsers();
    }

    loadUsers() {
        // Gets this instance of all the users and set the users variable
        // Will return an alertify error if there are any exceptions
        this.userService.getUsers().subscribe((users: User[]) => {
            this.users = users;
        }, error => {
            this.alertify.error(error);
        });
    }

}
