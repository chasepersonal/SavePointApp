import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { JwtModule } from '@auth0/angular-jwt';

import { AppComponent } from './app.component';
import { GamesComponent } from './games/games.component';
import { NavComponent } from './nav/nav.component';
import { AuthService } from './_services/auth.service';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { AlertifyService } from './_services/alertify.service';
import { MemberListComponent } from './member-list/member-list.component';
import { GroupListComponent } from './group-list/group-list.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './routes';
import { UserService } from './_services/user.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './_guards/auth.guard';

@NgModule({
    declarations: [
        AppComponent,
        GamesComponent,
        NavComponent,
        HomeComponent,
        RegisterComponent,
        MemberListComponent,
        GroupListComponent
    ],
    imports: [
        BrowserModule,
        // Used to retrieve HTTP requests
        HttpModule,
        HttpClientModule,
        FormsModule,
        JwtModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [
        AuthService,
        AlertifyService,
        UserService,
        AuthGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
