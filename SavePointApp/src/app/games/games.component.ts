import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {

  // Store game data into property
  games: any;

  // Inject HTTP module into constructor to allow HTTP responses
  constructor(private http: Http) { }

  ngOnInit() {
    this.getGames();
  }

  getGames() {

      // Log HTTP response to the console once it is fetched
      this.http.get('http://localhost:5500/api/games').subscribe(response => {
          this.games = response.json();
      });
  }
}
