import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { GamesComponent } from './games.component';

describe('GamesComponent', () => {
    let component: GamesComponent;
    let fixture: ComponentFixture<GamesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [GamesComponent],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GamesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should have getGames function', () => {
        expect(component.getGames).toBeTruthy();
    });
});
