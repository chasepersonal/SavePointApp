import { HomeComponent } from './home/home.component';
import { Routes } from '@angular/router';
import { MemberListComponent } from './member-list/member-list.component';
import { GamesComponent } from './games/games.component';
import { GroupListComponent } from './group-list/group-list.component';
import { AuthGuard } from './_guards/auth.guard';

// Route will match route with first available
// Wildcard is at bottom becuase it would always be executed otherwise
export const appRoutes: Routes = [
    
    { path: 'home', component: HomeComponent },
    // Guard routes for all logged in routes
    // Done through this custom route instead of adding canActivate: [AuthGuard] to each
    // Avoids DRY
    {
        path: '',
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuard],
        children: [
            { path: 'members', component: MemberListComponent },
            { path: 'games', component: GamesComponent },
            { path: 'groups', component: GroupListComponent },
        ]
    },
    // Wildcard path to redirect to home
    { path: '**', redirectTo: 'home', pathMatch: 'full' },
];
