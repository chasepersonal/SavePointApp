import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    model: any = {};

    @Input() games: any;
    // Declare output property for canceling registeration and assign new EventEmitter so it can perform an action
    @Output() cancelRegister = new EventEmitter;

    // Inject AuthService to authorize user for registration
    constructor(private authService: AuthService, private alertify: AlertifyService) { }

    ngOnInit() {
    }

    register() {
        this.authService.register(this.model).subscribe(() => {
            this.alertify.success('Registration successful');
        }, error => {
            this.alertify.error(error);
        });
    }

    cancel() {

        // Emit a boolean value of false to the cancelRegister output
        //
        this.cancelRegister.emit(false);
    }

}
