import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
    model: any = {};

    // Inject AuthService into constructor to allow successful login from the navbar
    constructor(private authService: AuthService, private alertify: AlertifyService, private router: Router) { }

    ngOnInit() {
    }

    login() {

        // Call auth service on login method
        // Login expects model to be passed
        // As method returns an observable, must return a subscribe
        // If subscribe is successfull, notify the console that the login was successful
        this.authService.login(this.model).subscribe(data => {
            // Call this instance of alertify success method to provide a success message to the user
            this.alertify.success('Logged In successfully');
        }, error => {
            // Call this instance of alertify error message to provide an error message to the user
            this.alertify.error('Failed to login');
            // Anonymous function to route to games page upon successfull login
        }, () => {
                this.router.navigate(['/games']);
        });       
    }

    logout() {
        // Set user token to null, then remove from local storage to log out user
        this.authService.userToken = null;
        localStorage.removeItem('token');
        // Call this instance of alertify
        this.alertify.message('Logged Out Successfully');
        // Route to the home page upon logging out
        this.router.navigate(['/games']);
    }

    loggedIn() {

        // Return the result of the token check from the auth services component
        // Result will then be published to the user
        return this.authService.loggedIn();
    }
}
