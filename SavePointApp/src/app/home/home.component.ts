import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    // Initialize registerMode to false
    // For purposes of displaying register form
    registerMode = false;

    games: any;

    constructor(private http: Http) { }

    ngOnInit() {
        
    }

    registerToggle() {

        // Shows register mode when function is called
        this.registerMode = true;
    }

    cancelRegisterMode(registerMode: boolean) {
        this.registerMode = registerMode;
    }
}
