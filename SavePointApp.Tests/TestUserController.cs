using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using SavePointApp.Controllers;
using SavePointApp.Data;
using SavePointApp.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SavePointApp.Tests
{
	[TestFixture]
    public class TestUserController
	{

		[Test]
        public async Task TestGetSingleUser()
		{
			//Arrange
			//Mock repository to test

			var mockGameRepo = new Mock<IGamesRepository>();

			// Setup a mock value to test
			mockGameRepo.Setup(repo => repo.GetUser(1)).ReturnsAsync(new User { Id = 1, Username = "Henry"});

			var controller = new UsersController(mockGameRepo.Object);

			var result = await controller.GetUser();

			Assert.IsNotNull(result);
        }
    }
}
